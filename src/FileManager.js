const fs = require('fs');

/**
 * Writes user to account JSON
 *
 * @param {number} id discord id 
 * @param {string} username uplay username
 * @param {string} uuid uplay uuid
 * @return {string} confirmation string
 */
export const writeToJSON = (id, username, uuid) => {
  const accounts = JSON.parse(fs.readFileSync('./src/accounts.json'))
  if (accounts[id]) { // already exists
    accounts[id].username = username
    accounts[id].uuid = uuid
  } else { // create
    accounts[id] = {}
    accounts[id].username = username
    accounts[id].uuid = uuid
  }
  fs.writeFileSync('./src/accounts.json', JSON.stringify(accounts))
  return `\nUtilisateur enregistré:\n\tid discord: ${id},\n\tcompte uplay: ${accounts[id].username},\n\tid uplay: ${accounts[id].uuid}`
}

/**
 * Read all accounts
 * @return {object} all accounts
 */
export const readAccounts = () => {
  const accounts = JSON.parse(fs.readFileSync('./src/accounts.json'))
  return accounts
}

/**
 * Get account UUID
 * @param {number} accountId account number
 * @return {string} account UUID
 */
export const getAccountUUID = (accountId) => {
  const accounts = readAccounts()
  if (!accounts[accountId]) {
    throw Error('no account set')
  }
  return accounts[accountId].uuid
}
