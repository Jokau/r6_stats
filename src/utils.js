import { WORKING_CHANNEL_NAME } from 'constants'

export const computeDuration = (start, end) => {
  const miliseconds = end - start
  let seconds = parseInt(miliseconds / 1000, 10)
  let minutes = 0
  let hours = 0

  while (seconds >= 60) {
    minutes += 1
    seconds -= 60
  }
  while (minutes >= 60) {
    hours += 1
  }
  return `${hours}:${minutes}:${seconds}`
}

export const compareSats = (oldStats, newStats) => {
  try {
    const matchesPlayed = `${newStats.matchesPlayed}(+${newStats.matchesPlayed - oldStats.matchesPlayed})`
    const kills = `${newStats.kills}(+${newStats.kills - oldStats.kills})`
    const deaths = `${newStats.deaths}(+${newStats.deaths - oldStats.deaths})`
    const wins = `${newStats.wins}(+${newStats.wins - oldStats.wins})`
    const losses = `${newStats.losses}(+${newStats.losses - oldStats.losses})`

    const kdRatioDiff = (newStats.kdRatio - oldStats.kdRatio).toFixed(4)
    const kdRatio = kdRatioDiff >= 0 ? `${newStats.kdRatio}(+${kdRatioDiff})` : `${newStats.kdRatio}(${kdRatioDiff})`
    const wlRatioDiff = (newStats.wlRatio - oldStats.wlRatio).toFixed(4)
    const wlRatio = wlRatioDiff >= 0 ? `${newStats.wlRatio}(+${wlRatioDiff})` : `${newStats.wlRatio}(${wlRatioDiff})`

    return `\tMatches joués : ${matchesPlayed}\tMatches gagnés : ${wins}\n\tMatches perdus : ${losses}\tRatio W/L : ${wlRatio}\n\tKills : ${kills}\tMorts : ${deaths}\tRatio K/D : ${kdRatio}`
  } catch (e) {
    throw new Error('Cannot parse new data')
  }
}

export const sendError = (Client, error) => {
  Client.channels.find('name', WORKING_CHANNEL_NAME).send("J'ai encore tout foiré, désolé...\n".concat(error))
}
