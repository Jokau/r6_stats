import JSSoup from 'jssoup'
import { getAccountUUID, readAccounts } from './FileManager'
import { compareSats, computeDuration } from './utils'

const fetch = require('node-fetch');

const state = {}
const BASE_URL = 'https://r6stats.com/stats/'

/**
 * Fetch data from r6stats.com
 * @param {number} userId stats user ID to fetch
 */
const fetchData = (userId) => {
  return new Promise((resolve, reject) => {
    const uuid = getAccountUUID(userId)
    fetch(BASE_URL.concat(uuid))
      .then(res => res.text())
      .then((result) => {
        const smallRes = result.split('<h4 class="card__title">Overall Stats</h4>')[1].split('<h4 class="card__title">Team Play</h4>')[0]
        const soup = new JSSoup(smallRes);
        const statsCounts = soup.findAll('span', 'stat-count')
        const statObject = {
          matchesPlayed: parseInt(statsCounts[1].nextElement._text.replace(',', ''), 10),
          kills: parseInt(statsCounts[3].nextElement._text.replace(',', ''), 10),
          deaths: parseInt(statsCounts[4].nextElement._text.replace(',', ''), 10),
          kdRatio: parseFloat(statsCounts[5].nextElement._text, 10),
          wlRatio: parseFloat(statsCounts[8].nextElement._text, 10),
          wins: parseInt(statsCounts[6].nextElement._text.replace(',', ''), 10),
          losses: parseInt(statsCounts[7].nextElement._text.replace(',', ''), 10),
        }
        resolve(statObject)
      })
      .catch(() => reject(Error('cannot fetch')))
  })
}

/**
 * Handle !start command
 * @param {Message} msg start message
 */
export const handleStart = (msg) => {
  const userId = msg.author.id
  if (!msg.author.bot) {
    if (!state[userId]) { // not already started
      // check account
      const accounts = readAccounts()
      if (!accounts[userId]) {
        // uplay account not set
        msg.reply('Configure d\'abord ton compte avec !set (!help pour avoir de l\'aide)')
        msg.delete()
      } else {
        // set current stats
        state[userId] = {}
        state[userId].startTime = new Date()
        state[userId].stats = {}
        fetchData(userId)
          .then((result) => {
            state[userId].stats = result
            msg.reply('C\'est parti! GL & HF!')
            msg.delete()
          })
          .catch((e) => {
            console.error(e)
            msg.reply('Quelque chose s\'est mal passé dans la requête désolé...')
            msg.delete()
          })
      }
    } else {
      msg.reply('Finis la session précédente avec !stop avant d\'en commencer une nouvelle')
      msg.delete()
    }
  }
}

/**
 * Handle !stop command
 * @param {Message} msg stop message
 */
export const handleStop = (msg) => {
  const userId = msg.author.id
  if (!msg.author.bot) {
    if (state[userId]) {
      const sessionTime = computeDuration(state[userId].startTime, new Date())
      fetchData(userId)
        .then((newStats) => {
          msg.reply('Je vais chercher les nouvelles données...') // TODO REMOVE DEBUG
          let result
          try {
            result = compareSats(state[userId].stats, newStats)
            delete state[userId] // close session
            msg.reply(`\nDurée de la session: ${sessionTime}\nStatitisques:\n${result}`)
            msg.delete()
          } catch (e) {
            console.error(e)
            msg.reply('Je me suis emmêlé les pinceaux avec toutes ces données, désolé... Mais tu peux réessayer')
          }
        })
        .catch((e) => {
          console.error(e)
          msg.reply('Je n\'arrive pas à récupérer les données désolé...')
          msg.delete()
        })
    } else {
      msg.reply('Commence d\'abord une session avec !start')
      msg.delete()
    }
  }
}
