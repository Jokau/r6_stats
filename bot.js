import { WORKING_CHANNEL_NAME, CMD_PREFIX } from './src/constants'
import { readAccounts, writeToJSON } from './src/FileManager'
import { handleStart, handleStop } from './src/SessionManager'

const Discord = require('discord.js')

require('dotenv').config()

const Client = new Discord.Client()

/**
 * Bot login using Token
 */
Client.login(process.env.BOT_TOKEN)
  .then(() => {
    console.log('Bot running...')
  })
  .catch((e) => {
    console.error(e)
  })

/**
 * On bot ready send message
 */
Client.once('ready', () => {
  Client.channels.find(channel => channel.name === WORKING_CHANNEL_NAME).send('Pour vous servir...')
})

/**
 * !set command to register uplay account
 */
const setAccount = (msg) => {
  const correctPattern = /^!set\s(\S*)\s(\S*)$/
  const result = msg.content.match(correctPattern)
  if (result) {
    const uuid = result[2].split('stats/')[1].replace('/', '')
    msg.reply(writeToJSON(msg.author.id, result[1], uuid))
  } else {
    msg.reply('Erreur de syntaxe : !set pseudo_uplay r6stats_url')
  }
}

/**
 *  Commands listener
 *    !help : displays help to set account
 *    !list : shows accounts set
 *    !start: starts session
 *    !stop : stops session
 */
Client.on('message', (msg) => {
  if (msg.channel.name === WORKING_CHANNEL_NAME
    && msg.content.startsWith(CMD_PREFIX)
    && !msg.author.bot) {
    switch (msg.content) {
      case '!help':
        msg.reply(`§1 : va sur https://r6stats.com
        §2 Entre ton pseudo Uplay dans la recherche et clique sur le résultat.
        §3 Récupère l'url : https://r6stats.com/stats/xxxxxxxx-xxxx-4xxx-xxxx-xxxxxxxxxxxx/)
        §4 tape !set pseudo_uplay r6stats_url`)
        msg.delete()
        return
      case '!list':
        msg.reply(JSON.stringify(readAccounts()))
        msg.delete()
        return
      case '!start':
        handleStart(msg)
        return
      case '!stop':
        handleStop(msg)
        return
      default:
        break
    }

    if (msg.content.startsWith('!set')) {
      setAccount(msg)
      msg.delete()
    }
  }
})

/**
 * Health check
 */
Client.setInterval(() => {
  console.log('TOUJOURS DEBOUT '.concat(new Date()))
}, 5 * 60 * 1000)
